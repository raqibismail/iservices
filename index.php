<!DOCTYPE html>
<html>

<head>
	<title>I-SERVICES System || Home Page</title>
	<link href='http://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<!-- Your custom CSS -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
</head>

<body>
	<?php include_once('includes/header.php'); ?>
	<div class="jumbotron intro">
		<div class="container">
			<h1 class="display-4">Welcome to I-SERVICES</h1>
			<p class="lead">We provide top-notch cleaning services for your home or office.</p>
		</div>
	</div>
	<div class="container">
		<div class="container-fluid p-3">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="images/cleaningitem.jpg" class="d-block w-100 rounded" alt="...">
						<div class="carousel-caption d-none d-md-block" style="background-color: rgba(0, 0, 0, 0.8); color: #fff; opacity: 1;">
							<h1>Cleaning</h1>
							<p style="font-size: 15px;">I-SERVICES helps you live smarter, giving you time to focus on what's most important.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="images/cleaning6.jpg" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block" style="background-color: rgba(0, 0, 0, 0.8); color: #fff; opacity: 1;">
							<p style="font-size: 15px;">We rigorously vet all of our staffs, who undergo identity checks as well as in-person interviews.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="images/cleaning2.jpg" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block" style="background-color: rgba(0, 0, 0, 0.8); color: #fff; opacity: 1;">
							<p style="font-size: 15px;">Our skilled staffs go above and beyond on every job. staffs are rated and reviewed after each task.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="images/cleaning3.jpg" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block" style="background-color: rgba(0, 0, 0, 0.8); color: #fff; opacity: 1;">
							<p style="font-size: 15px;">Online communication makes it easy for you to stay in touch with your staffs.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="images/cleaning5.jpg" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block" style="background-color: rgba(0, 0, 0, 0.8); color: #fff; opacity: 1;">
							<p style="font-size: 15px;">Pay securely online only when the cleaning is complete.</p>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
	<footer>
		<div class="container-flex">
			<?php include_once('includes/footer.php'); ?>
		</div>
	</footer>
	<script src="js/responsiveslides.min.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
	<script>
		$(function() {
			$("#slider").responsiveSlides({
				auto: true,
				manualControls: '#slider3-pager',
			});
		});
	</script>
	<script type="application/x-javascript">
		addEventListener("load", function() {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
</body>


</html>