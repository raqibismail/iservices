-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               11.1.2-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for housekeeping
CREATE DATABASE IF NOT EXISTS `housekeeping` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `housekeeping`;

-- Dumping structure for table housekeeping.tbladmin
CREATE TABLE IF NOT EXISTS `tbladmin` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `AdminName` varchar(120) DEFAULT NULL,
  `UserName` varchar(120) DEFAULT NULL,
  `MobileNumber` varchar(50) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Password` varchar(120) DEFAULT NULL,
  `AdminRegdate` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tbladmin: ~2 rows (approximately)
DELETE FROM `tbladmin`;
INSERT INTO `tbladmin` (`ID`, `AdminName`, `UserName`, `MobileNumber`, `Email`, `Password`, `AdminRegdate`) VALUES
	(1, 'Test', 'admin1', '5689784589', 'admin@gmail.com', 'f925916e2754e5e03f75dd58a5733251', '2020-01-21 11:48:13'),
	(2, 'admin', 'admin', '123492834', 'superadmin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '2023-10-27 13:31:13');

-- Dumping structure for table housekeeping.tblbooking
CREATE TABLE IF NOT EXISTS `tblbooking` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `BookingID` int(10) DEFAULT NULL,
  `ServiceID` int(10) DEFAULT NULL,
  `EventID` int(10) DEFAULT NULL,
  `UserID` int(10) DEFAULT NULL,
  `ServiceType` varchar(200) DEFAULT NULL,
  `ServiceDate` varchar(200) DEFAULT NULL,
  `ServiceStartingtime` varchar(200) DEFAULT NULL,
  `ServiceEndingtime` varchar(200) DEFAULT NULL,
  `VenueAddress` mediumtext DEFAULT NULL,
  `AdditionalInformation` mediumtext DEFAULT NULL,
  `BookingDate` timestamp NULL DEFAULT current_timestamp(),
  `Remark` varchar(200) DEFAULT NULL,
  `Status` varchar(200) DEFAULT NULL,
  `Assign` varchar(200) DEFAULT NULL,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`ID`),
  KEY `ServiceID` (`ServiceID`),
  KEY `Assign` (`Assign`),
  KEY `Status` (`Status`),
  KEY `ServiceType` (`ServiceType`),
  KEY `EventID` (`EventID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tblbooking: ~11 rows (approximately)
DELETE FROM `tblbooking`;
INSERT INTO `tblbooking` (`ID`, `BookingID`, `ServiceID`, `EventID`, `UserID`, `ServiceType`, `ServiceDate`, `ServiceStartingtime`, `ServiceEndingtime`, `VenueAddress`, `AdditionalInformation`, `BookingDate`, `Remark`, `Status`, `Assign`, `UpdationDate`) VALUES
	(1, 233064613, 3, 1, 1, 'Engagement', '2020-01-31', '2 p.m', '10 p.m', 'ABC park, Jawahar Nahar New Delhi ', 'jguytugvnvjhgh', '2020-01-24 12:46:29', 'Sheesh', '1', '3', '2023-11-05 11:25:00'),
	(2, 750016128, 3, 2, 2, 'Social', '2020-01-30', '2 p.m', '7 p.m', 'Harikesh Nagar Block C JKL park', 'kjhiuyughjvhsdadfs', '2020-01-28 05:44:37', 'Staff has been assigned', '1', '3', '2023-11-05 11:25:07'),
	(4, 206423586, 3, 4, 4, 'Sangeet', '2020-02-02', '11 a.m', '3 p.m', 'J-234 J&K block laxmi Nager,New Delhi', 'ghjgjhuywergcnxcjyhgfsdnbvxnzcgdsygtewghdfc', '2020-01-29 05:37:40', 'Test', '3', '2', '2023-11-05 11:25:09'),
	(5, 365319422, 5, 5, 1, 'Get Together', '2020-02-12', '7 p.m', '10 p.m', 'R-789 KW Raj Nagar Ghaziabad', 'xskjhj nbzxjhgagwejmb gdjswgdscbxzmnb', '2020-01-29 05:39:29', NULL, '2', '', '2023-11-05 11:25:09'),
	(6, 534626649, 6, 6, 2, 'Concert', '2020-01-31', '9 a.m', '4 p.m', 'T-980 Mahagun Noida Sector 62', 'gjydywetyuavxeweytugauygshghwgfdyasywsgdg', '2020-01-29 05:41:01', 'Test\r\n', '3', '2', '2023-11-05 11:25:10'),
	(8, 744853214, 4, 2, 4, 'Bungalow', '2023-10-12', '8 a.m', '9 a.m', 'Rumah Teres', 'Tadok', '2023-10-12 06:25:23', NULL, '3', '2', '2023-11-05 16:26:32'),
	(9, 310764631, 4, 3, 8, 'Bungalow', '2023-10-24', '10 a.m', '4 p.m', 'psmza', 'ok', '2023-10-23 02:02:17', NULL, '2', '2', '2023-11-05 16:26:42'),
	(10, 255264456, 8, 4, 9, 'Apartment', '2023-10-26', '8 a.m', '5 p.m', 'Pok Budu Atas', 'Mari derah sikit', '2023-10-25 03:20:55', 'Job has been assigned', '1', '2', '2023-11-05 11:25:29'),
	(11, 746839907, 3, 5, 10, 'Semi-D', '2023-10-25', '2 p.m', '4 p.m', 'Kajang', 'Please', '2023-10-29 09:03:24', NULL, '1', '2', '2023-11-05 16:13:50'),
	(12, 400594898, 3, 1, 2, 'Party', '2023-11-08', '10 a.m', '12 p.m', 'asdadasda', 'asdsadasadsa', '2023-11-05 11:56:22', 'Test', '1', '2', '2023-11-05 16:25:35'),
	(13, 766488736, 6, 2, 2, 'Party', '2023-11-10', '1 p.m', '3 p.m', 'Kajang', 'Bring a lot of bags.', '2023-11-05 16:18:05', 'Approved', '1', '3', '2023-11-05 16:21:13');

-- Dumping structure for table housekeeping.tblcontact
CREATE TABLE IF NOT EXISTS `tblcontact` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(225) DEFAULT NULL,
  `Email` varchar(225) DEFAULT NULL,
  `MobileNo` varchar(11) DEFAULT NULL,
  `Message` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `IsRead` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tblcontact: ~0 rows (approximately)
DELETE FROM `tblcontact`;
INSERT INTO `tblcontact` (`ID`, `Name`, `Email`, `MobileNo`, `Message`, `created_at`, `IsRead`) VALUES
	(1, 'MUHAMMAD RAQIB BIN ISMAIL', 'raqibkhs@gmail.com', '0135753975', 'Hello, test', '2023-11-05 12:09:59', 1);

-- Dumping structure for table housekeeping.tbleventtype
CREATE TABLE IF NOT EXISTS `tbleventtype` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `EventType` varchar(200) DEFAULT NULL,
  `CreationDate` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tbleventtype: ~6 rows (approximately)
DELETE FROM `tbleventtype`;
INSERT INTO `tbleventtype` (`ID`, `EventType`, `CreationDate`) VALUES
	(1, 'Bungalow', '2020-01-22 07:01:39'),
	(2, 'Semi-D', '2020-01-22 07:02:34'),
	(3, 'Terrace', '2020-01-22 07:02:43'),
	(4, 'Flat', '2020-01-22 07:03:00'),
	(5, 'Apartment', '2020-01-22 07:03:11'),
	(6, 'Condominium', '2023-10-04 06:03:42');

-- Dumping structure for table housekeeping.tblpage
CREATE TABLE IF NOT EXISTS `tblpage` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `PageType` varchar(100) DEFAULT NULL,
  `PageTitle` mediumtext DEFAULT NULL,
  `PageDescription` mediumtext DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tblpage: ~2 rows (approximately)
DELETE FROM `tblpage`;
INSERT INTO `tblpage` (`ID`, `PageType`, `PageTitle`, `PageDescription`, `Email`, `MobileNumber`, `UpdationDate`) VALUES
	(1, 'aboutus', 'About Us', 'We provide online booking, lightningfast customer service, and fantasticcleaning. We want to WOW you withoutstanding customer service,ecologically sustainable qualitycleaning products, no contractualobligations, pricing that beats thecompetitors and a 200% guaranteeevery time !', NULL, NULL, '2023-11-04 20:56:09'),
	(2, 'contactus', 'Contact Us', 'PSMZA, Dungun', 'info@gmail.com', 1234567890, '2023-10-04 06:04:48');

-- Dumping structure for table housekeeping.tblrole
CREATE TABLE IF NOT EXISTS `tblrole` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Dumping data for table housekeeping.tblrole: ~2 rows (approximately)
DELETE FROM `tblrole`;
INSERT INTO `tblrole` (`ID`, `name`) VALUES
	(1, 'Admin'),
	(2, 'Staff'),
	(3, 'User');

-- Dumping structure for table housekeeping.tblservice
CREATE TABLE IF NOT EXISTS `tblservice` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceName` varchar(200) DEFAULT NULL,
  `SerDes` varchar(250) NOT NULL,
  `ServicePrice` varchar(200) DEFAULT NULL,
  `CreationDate` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tblservice: ~5 rows (approximately)
DELETE FROM `tblservice`;
INSERT INTO `tblservice` (`ID`, `ServiceName`, `SerDes`, `ServicePrice`, `CreationDate`) VALUES
	(3, 'Folding Clothes', '(This service provides folding services for customer\'s clothes)', '50', '2020-01-24 07:19:14'),
	(4, 'Laundry', '(This service does the washing and ironing of clothes for customers. this service will wash, dry and iron your clothes.)', '50', '2020-01-24 07:19:51'),
	(5, 'Babysitting', '(Keeps children\'s living and play areas tidy. Helps with homework and tutoring as needed. Cares for infants, including feeding, diapering, and dressing. Meets the physical, social, and emotional needs of children in their care.)', '100', '2020-01-24 07:20:36'),
	(6, 'Cleaning Yards', '(This service does yard cleaning work by sweeping up trash, mowing the lawn and disposing of trash.)', '150', '2020-01-24 07:21:14'),
	(8, 'Cleaning', '(A cleaning performs services like sweeping, mopping, dusting and taking out the trash for residential or office environments.)', '150', '2023-10-24 17:41:28');

-- Dumping structure for table housekeeping.tblstaff
CREATE TABLE IF NOT EXISTS `tblstaff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone_no` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `role` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Dumping data for table housekeeping.tblstaff: ~3 rows (approximately)
DELETE FROM `tblstaff`;
INSERT INTO `tblstaff` (`ID`, `username`, `password`, `name`, `email`, `phone_no`, `role`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'admin@example.com', '0194627465', '1'),
	(2, 'staff1', '21232f297a57a5a743894a0e4a801fc3', 'Syafiqah', 'Syafiqah@gmail.com', '0194627334', '2'),
	(3, 'raqibismail', '81dc9bdb52d04dc20036dbd8313ed055', 'MUHAMMAD RAQIB BIN ISMAIL', 'raqib@gmail.com', '123456', '2');

-- Dumping structure for table housekeeping.tblstatus
CREATE TABLE IF NOT EXISTS `tblstatus` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tblstatus: ~3 rows (approximately)
DELETE FROM `tblstatus`;
INSERT INTO `tblstatus` (`ID`, `Name`) VALUES
	(1, 'Approved'),
	(2, 'Cancelled'),
	(3, 'Completed');

-- Dumping structure for table housekeeping.tbluser
CREATE TABLE IF NOT EXISTS `tbluser` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) DEFAULT NULL,
  `MobileNumber` varchar(11) DEFAULT NULL,
  `Username` varchar(200) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Role` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table housekeeping.tbluser: ~7 rows (approximately)
DELETE FROM `tbluser`;
INSERT INTO `tbluser` (`ID`, `Name`, `MobileNumber`, `Username`, `Password`, `Email`, `Role`) VALUES
	(1, 'Test', '7887878787', 'test1', '21232f297a57a5a743894a0e4a801fc3', 'test@gmail.com', '3'),
	(2, 'Poonam', '5545445444', 'poonam', '21232f297a57a5a743894a0e4a801fc3', 'poonam@yahoo.com', '3'),
	(4, 'Anuj', '9999857867', 'anuj', '21232f297a57a5a743894a0e4a801fc3', 'phpgurukulofficial@gmail.com', '3'),
	(8, 'John Doe', '1234567890', 'johndoe', '21232f297a57a5a743894a0e4a801fc3', 'john@example.com', '3'),
	(9, 'Jane Smith', '9876543210', 'janesmith', '21232f297a57a5a743894a0e4a801fc3', 'jane@example.com', '3'),
	(10, 'Alice Johnson', '5555555555', 'alice', '21232f297a57a5a743894a0e4a801fc3', 'alice@example.com', '3'),
	(14, 'MUHAMMAD RAQIB BIN ISMAIL', '0192837465', 'raqibismail', 'edcc1637565543050d2f9c3a7f25c3e6', 'raqibkhs@gmail.com', '3');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
