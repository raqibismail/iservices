<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');

if (isset($_POST['submit'])) {
	$name = $_POST['name'];
	$mobnum = $_POST['mobnum'];
	$email = $_POST['email'];
	$msg = $_POST['message'];
	$sql = "insert into tblcontact(Name,MobileNo,Email,Message)values(:name,:mobnum,:email,:msg)";
	$query = $dbh->prepare($sql);
	$query->bindParam(':name', $name, PDO::PARAM_STR);
	$query->bindParam(':mobnum', $mobnum, PDO::PARAM_STR);
	$query->bindParam(':email', $email, PDO::PARAM_STR);
	$query->bindParam(':msg', $msg, PDO::PARAM_STR);
	$query->execute();
	$LastInsertId = $dbh->lastInsertId();
	if ($LastInsertId > 0) {
		echo '<script>alert("Your Message Has Been Send. We Will Contact You Soon")</script>';
		echo "<script>window.location.href ='contact.php'</script>";
	} else {
		echo '<script>alert("Something Went Wrong. Please try again")</script>';
	}
}

?>
<!DOCTYPE html>
<html>

<head>
	<title>I-SERVICES System || Contact Us</title>
	<link href='http://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<!-- Your custom CSS -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all">

</head>

<body>
	<?php include_once('includes/header.php'); ?>
	<div class="contact content">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li class="active">Contact</li>
			</ol>
			<?php
			$sql = "SELECT * from tblpage where PageType='contactus'";
			$query = $dbh->prepare($sql);
			$query->execute();
			$results = $query->fetchAll(PDO::FETCH_OBJ);

			$cnt = 1;
			if ($query->rowCount() > 0) {
				foreach ($results as $row) {               ?>
					<h2><?php echo htmlentities($row->PageTitle); ?></h2>
					<div class="contact-main">
						<h4 style="color: white"><span class="glyphicon glyphicon-home" aria-hidden="true"> <?php echo htmlentities($row->PageDescription); ?></h4>
						<br>
						<h4 style="color: white"><span class="glyphicon glyphicon-envelope" aria-hidden="true"> <?php echo htmlentities($row->Email); ?></h4>
						<br>
						<h4 style="color: white"><span class="glyphicon glyphicon-phone" aria-hidden="true"> <?php echo htmlentities($row->MobileNumber); ?></h4>
				<?php $cnt = $cnt + 1;
				}
			} ?>

				<div class="row py-3">
					<div class="col-md-12 text-white">
						<p>Or drop a message and we will reply to you soon.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<form method="post">
							<div class="form-group">
								<label for="name" class="text-white font-weight-normal">Name:</label>
								<input type="text" class="form-control" id="name" name="name" required="true">
							</div>
							<div class="form-group">
								<label for="email" class="text-white font-weight-normal">Email:</label>
								<input type="text" class="form-control" id="email" name="email" required="true">
							</div>
							<div class="form-group">
								<label for="mobnum" class="text-white font-weight-normal">Mobile Number:</label>
								<input type="text" class="form-control" id="mobnum" name="mobnum" required="true" maxlength="10" pattern="[0-9]+">
							</div>
							<div class="form-group">
								<label for="message" class="text-white font-weight-normal">Message:</label>
								<textarea class="form-control" id="message" name="message" required="true"></textarea>
							</div>
							<div class="form-group pt-2">
								<input type="submit" name="submit" value="Submit" class="btn btn-primary">
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<div class="contact-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d127227.69585656234!2d103.31866909999998!3d4.793125350000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31c805887d316621%3A0xab269c5406262890!2sKuala%20Dungun%2C%20Terengganu!5e0!3m2!1sen!2smy!4v1698171023895!5m2!1sen!2smy" width="800" height="600" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"> </iframe>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
					</div>
		</div>
	</div>
	<!---->
	<footer>
		<div class="container-flex">
			<?php include_once('includes/footer.php'); ?>
		</div>
	</footer>
	<!---->
	<script src="js/jquery.min.js"></script>
	<script type="application/x-javascript">
		addEventListener("load", function() {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--webfont-->
	<!---//End-css-style-switecher----->
	<script type="text/javascript" src="js/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

		});
	</script>

</body>

</html>