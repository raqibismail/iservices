<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['odmsaid'] == 0)) {
    header('location:logout.php');
} else {



?>
    <!doctype html>
    <html lang="en" class="no-focus"> <!--<![endif]-->

    <head>
        <title>I-SERVICES</title>

        <link rel="stylesheet" href="assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <link rel="stylesheet" id="css-main" href="assets/css/codebase.min.css">

    </head>

    <body>

        <div id="page-container" class="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-narrow">

            <?php include_once('includes/sidebar.php'); ?>

            <?php include_once('includes/header.php'); ?>


            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Search Booking</h2>



                    <!-- Dynamic Table Full Pagination -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Search Booking</h3>
                        </div>
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
                            <form id="basic-form" method="post">
                                <div class="form-group">
                                    <label>Search by Booking No./Name/Mobile No.</label>
                                    <input id="searchdata" type="text" name="searchdata" required="true" class="form-control" placeholder="Booking No./Name/Mobile No.">
                                </div>

                                <br>
                                <button type="submit" class="btn btn-primary" name="search" id="submit">Search</button>
                            </form>
                            <?php
                            if (isset($_POST['search'])) {

                                $sdata = $_POST['searchdata'];
                            ?>
                                <h4 align="center">Result against "<?php echo $sdata; ?>" keyword </h4>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                                    <thead>
                                        <tr>
                                            <th class="text-center"></th>
                                            <th>Booking ID</th>
                                            <th class="d-none d-sm-table-cell">Cutomer Name</th>
                                            <th class="d-none d-sm-table-cell">Mobile Number</th>
                                            <th class="d-none d-sm-table-cell">Email</th>
                                            <th class="d-none d-sm-table-cell">Booking Date</th>
                                            <th class="d-none d-sm-table-cell">Status</th>
                                            <th class="d-none d-sm-table-cell" style="width: 15%;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT b.*, u.Name AS UserName, u.MobileNumber AS UserMobile, u.Email AS UserEmail 
                                                FROM tblbooking AS b
                                                LEFT JOIN tbluser AS u ON b.userID = u.ID
                                                WHERE b.BookingID LIKE '$sdata%' 
                                                OR u.Name LIKE '$sdata%' 
                                                OR u.MobileNumber LIKE '$sdata%'
                                                OR u.Email LIKE '$sdata%'";
                                        $query = $dbh->prepare($sql);
                                        $query->execute();
                                        $results = $query->fetchAll(PDO::FETCH_OBJ);

                                        $cnt = 1;
                                        if ($query->rowCount() > 0) {
                                            foreach ($results as $row) {               ?>
                                                <tr>
                                                    <td class="text-center"><?php echo htmlentities($cnt); ?></td>
                                                    <td class="font-w600"><?php echo htmlentities($row->BookingID); ?></td>
                                                    <td class="font-w600"><?php echo htmlentities($row->UserName); ?></td>
                                                    <td class="font-w600"><?php echo htmlentities($row->UserMobile); ?></td>
                                                    <td class="font-w600"><?php echo htmlentities($row->UserEmail); ?></td>
                                                    <td class="font-w600">
                                                        <span class="badge badge-primary"><?php echo htmlentities($row->BookingDate); ?></span>
                                                    </td>
                                                    <?php if ($row->Status == "") { ?>

                                                        <td class="font-w600"><?php echo "Not Updated Yet"; ?></td>
                                                    <?php } else { ?>
                                                        <td class="d-none d-sm-table-cell">
                                                            <?php
                                                            $statusId = $row->Status;
                                                            $sql = "SELECT * FROM tblstatus WHERE ID=:id";
                                                            $query = $dbh->prepare($sql);
                                                            $query->bindParam(":id", $statusId, PDO::PARAM_INT);
                                                            $query->execute();
                                                            $row2 = $query->fetch(PDO::FETCH_OBJ);

                                                            $badgeClass = 'badge-secondary';

                                                            if ($row2->Name == 'Approved') {
                                                                $badgeClass = 'badge-primary';
                                                            } elseif ($row2->Name == 'Cancelled') {
                                                                $badgeClass = 'badge-danger';
                                                            } elseif ($row2->Name == 'Completed') {
                                                                $badgeClass = 'badge-success';
                                                            }

                                                            echo '<span class="badge ' . $badgeClass . '">' . htmlentities($row2->Name) . '</span>';
                                                            ?>
                                                        </td>
                                                    <?php } ?>
                                                    <td class="d-none d-sm-table-cell"><a href="view-booking-detail.php?editid=<?php echo htmlentities($row->ID); ?>&&bookingid=<?php echo htmlentities($row->BookingID); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                                </tr>




                                    </tbody>
                                <?php
                                                $cnt = $cnt + 1;
                                            }
                                        } else { ?>
                                <tr>
                                    <td colspan="8"> No record found against this search</td>

                                </tr>
                        <?php }
                                    } ?>
                                </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full Pagination -->

                    <!-- END Dynamic Table Simple -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <?php include_once('includes/footer.php'); ?>
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="assets/js/core/jquery.min.js"></script>
        <script src="assets/js/core/popper.min.js"></script>
        <script src="assets/js/core/bootstrap.min.js"></script>
        <script src="assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="assets/js/core/jquery.appear.min.js"></script>
        <script src="assets/js/core/jquery.countTo.min.js"></script>
        <script src="assets/js/core/js.cookie.min.js"></script>
        <script src="assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page JS Code -->
        <script src="assets/js/pages/be_tables_datatables.js"></script>
    </body>

    </html>
<?php }  ?>