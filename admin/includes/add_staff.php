<?php
include('dbconnection.php');

if (isset($_POST['addStaff'])) {
    // Handle the form submission to add a new staff member
    $username = $_POST['username'];
    $name = $_POST['name'];
    $icnum = $_POST['icnum'];
    $role = $_POST['role'];
    $phone_no = $_POST['phone_no'];
    $password = md5($icnum);

    // Check if the username is already taken
    $checkUsernameSQL = "SELECT username FROM tblstaff WHERE username = :username";
    $checkQuery = $dbh->prepare($checkUsernameSQL);
    $checkQuery->bindParam(':username', $username, PDO::PARAM_STR);
    $checkQuery->execute();

    if ($checkQuery->rowCount() > 0) {
        // Username is already taken, show an error message
        echo "<script type='text/javascript'>alert('Username has been taken!');</script>";
        echo "<script type='text/javascript'> document.location = '../manage-staff.php'; </script>";
    } else {
        // Username is available, proceed with the insert
        $sql = "INSERT INTO tblstaff (username, password, name, role, phone_no) VALUES (:username, :password, :name, :role, :phone_no)";
        $query = $dbh->prepare($sql);
        $query->bindParam(':username', $username, PDO::PARAM_STR);
        $query->bindParam(':name', $name, PDO::PARAM_STR);
        $query->bindParam(':password', $password, PDO::PARAM_STR);
        $query->bindParam(':role', $role, PDO::PARAM_STR);
        $query->bindParam(':phone_no', $phone_no, PDO::PARAM_STR);
        $query->execute();

        echo "<script type='text/javascript'> document.location = '../manage-staff.php'; </script>";
    }
}
?>