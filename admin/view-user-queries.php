<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['odmsaid'] == 0)) {
  header('location:logout.php');
} else {

  $vid = $_GET['viewid'];

?>
  <!doctype html>
  <html lang="en" class="no-focus"> <!--<![endif]-->

  <head>
    <title>I-SERVICES - View Queries</title>

    <link rel="stylesheet" href="assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" id="css-main" href="assets/css/codebase.min.css">

  </head>

  <body>

    <div id="page-container" class="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-narrow">

      <?php include_once('includes/sidebar.php'); ?>

      <?php include_once('includes/header.php'); ?>


      <!-- Main Container -->
      <main id="main-container">
        <!-- Page Content -->
        <div class="content">
          <h2 class="content-heading">View Queries</h2>



          <!-- Dynamic Table Full Pagination -->
          <div class="block">
            <div class="block-header block-header-default">
              <h3 class="block-title">View Queries</h3>
            </div>
            <div class="block-content block-content-full">
              <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->

              <?php

              $sql = "SELECT * from  tbluser where ID=$vid";
              $query = $dbh->prepare($sql);
              $query->execute();
              $results = $query->fetchAll(PDO::FETCH_OBJ);
              $cnt = 1;
              if ($query->rowCount() > 0) {
                foreach ($results as $row) {

                  $timestamp = strtotime($row->created_at);
                  $formattedDate = date("F j, Y, g:i a", $timestamp); ?>

                  <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                    <tr align="center">
                      <td colspan="4" style="font-size:20px;color:blue">
                        View Queries</td>
                    </tr>

                    <tr>
                      <th scope>Name</th>
                      <td><?php echo ($row->Name); ?></td>
                      <th scope>Username</th>
                      <td><?php echo ($row->Username); ?></td>
                    </tr>
                    <tr>
                      <th scope>Email</th>
                      <td><?php echo ($row->Email); ?></td>
                      <th scope>Mobile Number</th>
                      <td><?php echo ($row->MobileNumber); ?></td>
                    </tr>


                <?php $cnt = $cnt + 1;
                }
              } ?>
                  </table>
            </div>
          </div>
          <!-- END Dynamic Table Full Pagination -->

          <!-- END Dynamic Table Simple -->
        </div>
        <!-- END Page Content -->
      </main>
      <!-- END Main Container -->

      <?php include_once('includes/footer.php'); ?>
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="assets/js/core/jquery.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <script src="assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="assets/js/core/jquery.appear.min.js"></script>
    <script src="assets/js/core/jquery.countTo.min.js"></script>
    <script src="assets/js/core/js.cookie.min.js"></script>
    <script src="assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page JS Code -->
    <script src="assets/js/pages/be_tables_datatables.js"></script>
  </body>

  </html>
<?php }  ?>