book-services.php<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');

if (isset($_POST['submit'])) {
	$bid = $_GET['bookid'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$username = $_POST['username'];

	// Use password_hash for secure password hashing
	$password = md5($_POST['password']);

	$mobnum = $_POST['mobnum'];
	$role = 3; // You can customize this based on your role system

	// Validate and sanitize input to prevent SQL injection
	// ...

	$sql = "INSERT INTO tbluser (Name, MobileNumber, Username, Password, Email, Role) VALUES (:name, :mobnum, :username, :password, :email, :role)";
	$query = $dbh->prepare($sql);
	$query->bindParam(':name', $name, PDO::PARAM_STR);
	$query->bindParam(':mobnum', $mobnum, PDO::PARAM_STR);
	$query->bindParam(':username', $username, PDO::PARAM_STR);
	$query->bindParam(':password', $password, PDO::PARAM_STR);
	$query->bindParam(':email', $email, PDO::PARAM_STR);
	$query->bindParam(':role', $role, PDO::PARAM_STR);

	// Execute the query
	$query->execute();

	$LastInsertId = $dbh->lastInsertId();

	if ($LastInsertId > 0) {
		echo '<script>alert("Your Account has been registered. Please login to continue.")</script>';
		echo "<script>window.location.href ='login.php'</script>";
	} else {
		echo '<script>alert("Something Went Wrong. Please try again")</script>';
	}
}
?>
<!DOCTYPE html>
<html>

<head>
	<title>I-SERVICES System || Registration</title>
	<link href='http://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<!-- Your custom CSS -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
</head>

<body>
	<?php include_once('includes/header.php'); ?>
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li class="active">Registration</li>
		</ol>

		<div class="text-white">
			<p>Register your account now. </p>
		</div>
		<div class="container">
			<form class="form text-white pt-3" method="post">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group text-white">
							<label class="font-weight-normal" for="name">Name:</label>
							<input type="text" class="form-control" name="name" required="true">
						</div>
						<div class="form-group text-white">
							<label class="font-weight-normal" for="email">Email:</label>
							<input type="email" class="form-control" name="email" required="true">
						</div>
						<div class="form-group text-white">
							<label class="font-weight-normal" for="name">Username:</label>
							<input type="text" class="form-control" name="username" required="true">
						</div>
						<div class="form-group text-white">
							<label class="font-weight-normal" for="password">Password:</label>
							<input type="password" class="form-control" name="password" required="true">
						</div>
						<div class="form-group text-white">
							<label class="font-weight-normal" for="mobnum">Mobile Number:</label>
							<input type="text" class="form-control" name="mobnum" required="true" maxlength="10" pattern="[0-9]+">
						</div>
						<div class="form-group">
							<div class="text-center pt-3">
								<button class="btn btn-primary text-center" type="submit" name="submit" value="Book">Submit</button>
							</div>
						</div>
					</div>
			</form>
		</div>
	</div>

	</div>
	<!-- <div class="col-md-6 contact-right">
					<div class="contact-map">
						<img src="images/cleaningcrew.jpg" class="img-responsive" height="900" width="500" alt="" />
					</div>
				</div> -->
	<div class="clearfix"></div>

	<footer class="b">
		<div class="container-flex">
			<?php include_once('includes/footer.php'); ?>
		</div>
	</footer>
	<!---->

	<!---->
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.js"></script>
	<script type="application/x-javascript">
		addEventListener("load", function() {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

		});
	</script>
</body>


</html>