<?php
session_start();
error_reporting(0);
include('dbconnection.php');

if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    // Check the tblstaff table for staff login
    $sqlStaff = "SELECT ID, role FROM tblstaff WHERE username = :username AND password = :password";
    $queryStaff = $dbh->prepare($sqlStaff);
    $queryStaff->bindParam(':username', $username, PDO::PARAM_STR);
    $queryStaff->bindParam(':password', $password, PDO::PARAM_STR);
    $queryStaff->execute();
    $staffResult = $queryStaff->fetch(PDO::FETCH_OBJ);

    // Check the tbluser table for user login
    $sqlUser = "SELECT ID, Role FROM tbluser WHERE Username = :username AND Password = :password";
    $queryUser = $dbh->prepare($sqlUser);
    $queryUser->bindParam(':username', $username, PDO::PARAM_STR);
    $queryUser->bindParam(':password', $password, PDO::PARAM_STR);
    $queryUser->execute();
    $userResult = $queryUser->fetch(PDO::FETCH_OBJ);

    if ($staffResult) {
        // Staff login
        $_SESSION['odmsaid'] = $staffResult->ID;
        if (!empty($_POST["remember"])) {
            // Set cookies for username and password if the "Remember Me" checkbox is checked
            setcookie("user_login", $_POST["username"], time() + (10 * 365 * 24 * 60 * 60));
            setcookie("userpassword", $_POST["password"], time() + (10 * 365 * 24 * 60 * 60));
        } else {
            // Clear cookies if "Remember Me" is unchecked
            if (isset($_COOKIE["user_login"])) {
                setcookie("user_login", "");
            }
            if (isset($_COOKIE["userpassword"])) {
                setcookie("userpassword", "");
            }
        }
        $_SESSION['login'] = $_POST['username'];
        // Redirect to different pages based on the user's role
        if ($staffResult->role === '1') {
            echo "<script type='text/javascript'> document.location = '../../admin/dashboard.php'; </script>";
        } elseif ($staffResult->role === '2') {
            echo "<script type='text/javascript'> document.location = '../../staff/dashboard.php'; </script>";
        }
    } elseif ($userResult) {
        // User login
        $_SESSION['odmsaid'] = $userResult->ID;
        if (!empty($_POST["remember"])) {
            // Set cookies for username and password if the "Remember Me" checkbox is checked
            setcookie("user_login", $_POST["username"], time() + (10 * 365 * 24 * 60 * 60));
            setcookie("userpassword", $_POST["password"], time() + (10 * 365 * 24 * 60 * 60));
        } else {
            // Clear cookies if "Remember Me" is unchecked
            if (isset($_COOKIE["user_login"])) {
                setcookie("user_login", "");
            }
            if (isset($_COOKIE["userpassword"])) {
                setcookie("userpassword", "");
            }
        }
        $_SESSION['login'] = $_POST['username'];
        // Redirect to different pages based on the user's role
        if ($userResult->Role === '3') {
            echo "<script type='text/javascript'> document.location = '../../user/dashboard.php'; </script>";
        }
    } else {
        // Display an error message for invalid credentials
        echo "<script>alert('Invalid Details');</script>";
        echo "<script type='text/javascript'> document.location = '../login.php'; </script>";

    }
}
