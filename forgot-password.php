<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');

if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];
    $newpassword = md5($_POST['newpassword']);
    $sql = "SELECT Email FROM tbluser WHERE Email=:email and MobileNumber=:mobile";
    $query = $dbh->prepare($sql);
    $query->bindParam(':email', $email, PDO::PARAM_STR);
    $query->bindParam(':mobile', $mobile, PDO::PARAM_STR);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_OBJ);
    if ($query->rowCount() > 0) {
        $con = "update tbluser set Password=:newpassword where Email=:email and MobileNumber=:mobile";
        $chngpwd1 = $dbh->prepare($con);
        $chngpwd1->bindParam(':email', $email, PDO::PARAM_STR);
        $chngpwd1->bindParam(':mobile', $mobile, PDO::PARAM_STR);
        $chngpwd1->bindParam(':newpassword', $newpassword, PDO::PARAM_STR);
        $chngpwd1->execute();
        echo "<script>alert('Your Password succesfully changed');</script>";
    } else {
        echo "<script>alert('Email id or Mobile no is invalid');</script>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Online Banquet Booking System | Forgot Password</title>
    <link href='http://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Your custom CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all">

</head>

<body>
    <!-- banner -->
    <?php include_once('includes/header.php'); ?>
    <!-- //banner -->
    <!-- contact -->
    <div class="content">
        <div class="contact">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="login.php">Login</a></li>
                    <li class="active">Forgot Password</li>
                </ol>
                <div class="agile-contact-form">
                    <div class="row">
                        <div class="col-md-6 contact-form-left">
                            <div class="agileits-contact-address">
                                <img class="rounded" src="images/housekeeper.jpg" alt="" height="500" width="500">
                            </div>
                        </div>
                        <div class="col-md-6 contact-form-right">
                            <div class="contact-form-top text-white pb-3">
                                <h3>Reset Your Password</h3>
                            </div>
                            <div class="agileinfo-contact-form-grid">
                                <form action="#" method="post" name="chngpwd" onsubmit="return valid();">
                                    <div class="form-group">
                                        <label for="email" class="text-white font-weight-normal">E-mail</label>
                                        <input type="email" class="form-control" name="email" placeholder="E-mail" required="true">
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile" class="text-white font-weight-normal">Mobile Number</label>
                                        <input type="text" class="form-control" required="true" name="mobile" maxlength="10" pattern="[0-9]+" placeholder="Mobile Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="newpassword" class="text-white font-weight-normal">New Password</label>
                                        <input type="password" class="form-control" name="newpassword" placeholder="New Password" required="true" />
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmpassword" class="text-white font-weight-normal">Confirm Password</label>
                                        <input type="password" name="confirmpassword" placeholder="Confirm Password" class="form-control" required="true" />
                                    </div>
                                    <div class="form-group">
                                        <a class="text-white font-weight-normal" href="login.php">Already have an account?</a>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-primary px-4" name="submit">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //contact -->
    <footer>
        <div class="container-flex">
            <?php include_once('includes/footer.php'); ?>
        </div>
    </footer>

    <!-- jarallax -->
    <script src="js/jarallax.js"></script>
    <script src="js/SmoothScroll.min.js"></script>
    <script type="text/javascript">
        /* init Jarallax */
        $('.jarallax').jarallax({
            speed: 0.5,
            imgWidth: 1366,
            imgHeight: 768
        })
    </script>
    <!-- //jarallax -->
    <script src="js/SmoothScroll.min.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <!-- //here ends scrolling icon -->
    <script src="js/modernizr.custom.js"></script>
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
    <script type="text/javascript">
        function valid() {
            if (document.chngpwd.newpassword.value != document.chngpwd.confirmpassword.value) {
                alert("New Password and Confirm Password Field do not match  !!");
                document.chngpwd.confirmpassword.focus();
                return false;
            }
            return true;
        }
    </script>

</body>

</html>