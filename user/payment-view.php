<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['odmsaid'] == 0)) {
    header('location:logout.php');
} else {

?>
    <!doctype html>
    <html lang="en" class="no-focus"> <!--<![endif]-->

    <head>
        <title>I-SERVICES - View Payment</title>
        <link rel="stylesheet" id="css-main" href="assets/css/codebase.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.4.0/jspdf.umd.min.js"></script>
    </head>

    <body>
        <div id="page-container" class="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-narrow">


            <?php include_once('includes/sidebar.php'); ?>

            <?php include_once('includes/header.php'); ?>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">

                    <!-- Register Forms -->
                    <h2 class="content-heading">View Payment</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Bootstrap Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-gd-emerald">
                                    <h3 class="block-title">View Payment</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                            <i class="si si-refresh"></i>
                                        </button>
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                                    </div>
                                </div>
                                <p><?php echo $userID; ?></p>
                                <div class="block-content">

                                    <?php
                                    $latestPaymentID = $_GET['id'];

                                    $sql = "SELECT * FROM tblpayment WHERE paymentID = :latestID";
                                    $query = $dbh->prepare($sql);
                                    $query->bindParam(':latestID', $latestPaymentID, PDO::PARAM_STR);
                                    $query->execute();
                                    $results = $query->fetchAll(PDO::FETCH_OBJ);
                                    $cnt = 1;
                                    if ($query->rowCount() > 0) {
                                        foreach ($results as $row) {
                                            $sqlUser = "SELECT * FROM tbluser WHERE ID = :userID";
                                            $queryUser = $dbh->prepare($sqlUser);
                                            $queryUser->bindParam(':userID', $row->userID, PDO::PARAM_INT);
                                            $queryUser->execute();
                                            $userData = $queryUser->fetch(PDO::FETCH_OBJ);

                                    ?>
                                            <table border="1" class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                                                <tr>
                                                    <th>Payment ID</th>
                                                    <td><?php echo $row->paymentID; ?></td>
                                                    <th>Client Name</th>
                                                    <td><?php echo $userData->Name; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Mobile Number</th>
                                                    <td><?php echo $userData->MobileNumber; ?></td>
                                                    <th>Email</th>
                                                    <td><?php echo $userData->Email; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Card Name</th>
                                                    <td><?php echo $row->cardName; ?></td>
                                                    <th>Card Number</th>
                                                    <td><?php echo str_replace(' ', '', $row->cardNumber);; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Service Price</th>
                                                    <td>RM<?php echo $row->priceTotal; ?></td>
                                                    <th>Payment Date</th>
                                                    <td><?php echo date("F j, Y g:i a", strtotime($row->paymentDate)); ?></td>
                                                </tr>
                                            <?php
                                            $cnt = $cnt + 1;
                                        }
                                            ?>
                                            </table>
                                            <?php

                                            //if ($assign == "") {
                                            ?>
                                            <!-- <p align="center" style="padding-top: 20px">
                        <button class="btn btn-primary waves-effect waves-light w-lg" data-toggle="modal" data-target="#myModal">Take Action</button>
                      </p> -->

                                            <?php //} 
                                            ?>



                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Take Action</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-bordered table-hover data-tables">

                                                                <form method="post" name="submit">
                                                                    <tr>
                                                                        <th>Status :</th>
                                                                        <td>

                                                                            <select name="status" class="form-control wd-450" required="true">
                                                                                <?php
                                                                                $statId = $row->Status;
                                                                                $sql = "SELECT * from tblstatus WHERE ID=:id";
                                                                                $query = $dbh->prepare($sql);
                                                                                $query->bindParam(':id', $statId, PDO::PARAM_INT);
                                                                                $query->execute();
                                                                                $currentStatus = $query->fetch(PDO::FETCH_ASSOC);


                                                                                if ($row->Status) echo '<option value="' . $currentStatus['ID'] . '" selected>' . $currentStatus['Name'] . '</option>';
                                                                                else echo '<option value="" selected>Select Status</option>';


                                                                                $sql = "SELECT * from tblstatus WHERE ID != :id";
                                                                                $query = $dbh->prepare($sql);
                                                                                $query->bindParam(':id', $statId, PDO::PARAM_INT);
                                                                                $query->execute();
                                                                                $allStatus = $query->fetchAll(PDO::FETCH_ASSOC);

                                                                                foreach ($allStatus as $stat) {
                                                                                    echo '<option value="' . $stat['ID'] . '">' . $stat['Name'] . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <th>Assign :</th>

                                                                        <td>
                                                                            <select name="assign" class="form-control wd-450">
                                                                                <?php
                                                                                $sid = $row->Assign;
                                                                                $role = '2';
                                                                                $sql = "SELECT * FROM tblstaff WHERE ID=:id ";
                                                                                $query = $dbh->prepare($sql);
                                                                                $query->bindParam(':id', $sid, PDO::PARAM_INT);
                                                                                $query->execute();
                                                                                $currentStaff = $query->fetch(PDO::FETCH_ASSOC);

                                                                                if ($row->Assign) echo '<option class="text-muted" value="' . $currentStaff['ID'] . '" selected>' . $currentStaff['name'] . '</option>';
                                                                                else echo '<option value="" selected>Select a Staff</option>';

                                                                                if ($row->Assign) {
                                                                                    $sql = "SELECT * FROM tblstaff WHERE ID != :id AND role=:staffRole ";
                                                                                    $query = $dbh->prepare($sql);
                                                                                    $query->bindParam(':id', $sid, PDO::PARAM_INT);
                                                                                    $query->bindParam(':staffRole', $role, PDO::PARAM_STR);
                                                                                } else {
                                                                                    $sql = "SELECT * FROM tblstaff WHERE ID = :id AND role='2' ";
                                                                                    $query = $dbh->prepare($sql);
                                                                                    $query->bindParam(':id', $_SESSION['odmsaid'], PDO::PARAM_INT);
                                                                                }

                                                                                $query->execute();
                                                                                $staffMembers = $query->fetchAll(PDO::FETCH_ASSOC);

                                                                                foreach ($staffMembers as $staffMember) {
                                                                                    echo '<option value="' . $staffMember['ID'] . '">' . $staffMember['name'] . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </td>
                                                                    </tr>

                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" name="submit" class="btn btn-primary">Update</button>

                                                            </form>



                                                        </div>
                                                    </div>
                                                    <!-- END Bootstrap Register -->
                                                </div>

                                            </div>
                                        <?php } ?>

                                </div>
                                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <?php include_once('includes/footer.php'); ?>
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="assets/js/core/jquery.min.js"></script>
        <script src="assets/js/core/popper.min.js"></script>
        <script src="assets/js/core/bootstrap.min.js"></script>
        <script src="assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="assets/js/core/jquery.appear.min.js"></script>
        <script src="assets/js/core/jquery.countTo.min.js"></script>
        <script src="assets/js/core/js.cookie.min.js"></script>
        <script src="assets/js/codebase.js"></script>

        <!-- JavaScript code for PDF generation -->
        <!-- JavaScript code for PDF generation -->
        <!-- JavaScript code for PDF generation -->
        <!-- JavaScript code for PDF generation -->
    </body>

    </html>
<?php }  ?>