<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['odmsaid'] == 0)) {
  header('location:logout.php');
} else {
  if (isset($_POST['submit'])) {


    $eid = $_GET['viewid'];
    $bookingid = $_GET['bookingid'];
    $status = $_POST['status'];
    $remark = $_POST['remark'];
    $assign = $_POST['assign'];


    $sql = "update tblbooking set Status=:status,Remark=:remark,Assign=:assign where ID=:eid";
    $query = $dbh->prepare($sql);
    $query->bindParam(':status', $status, PDO::PARAM_STR);
    $query->bindParam(':remark', $remark, PDO::PARAM_STR);
    $query->bindParam(':assign', $assign, PDO::PARAM_STR);
    $query->bindParam(':eid', $eid, PDO::PARAM_STR);

    $query->execute();

    echo '<script>alert("Remark has been updated")</script>';
    echo "<script>window.location.href ='assigned-task.php'</script>";
  }

?>
  <!doctype html>
  <html lang="en" class="no-focus"> <!--<![endif]-->

  <head>
    <title>I-SERVICES - View Booking</title>
    <link rel="stylesheet" id="css-main" href="assets/css/codebase.min.css">
    <script src="https://rawgit.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
  </head>

  <body>
    <div id="page-container" class="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-narrow">


      <?php include_once('includes/sidebar.php'); ?>

      <?php include_once('includes/header.php'); ?>

      <!-- Main Container -->
      <!-- Page Content -->
      <div class="content">

        <!-- Register Forms -->
        <main id="main-container">
          <h2 class="content-heading">View Booking</h2>
          <div class="row">
            <div class="col-md-12">
              <!-- Bootstrap Register -->
              <div class="block block-themed" id="downloadPdf">
                <div class="block-header bg-gd-emerald">
                  <h3 class="block-title">View Booking</h3>
                  <div class="block-options">
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                      <i class="si si-refresh"></i>
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                  </div>
                </div>
                <div class="block-content">

                  <?php
                  $eid = $_GET['viewid'];

                  $sql = "SELECT * 
                          FROM tblbooking 
                          JOIN tblservice ON tblbooking.ServiceID = tblservice.ID 
                          WHERE tblbooking.ID = :eid";
                  $query = $dbh->prepare($sql);
                  $query->bindParam(':eid', $eid, PDO::PARAM_STR);
                  $query->execute();
                  $results = $query->fetchAll(PDO::FETCH_OBJ);

                  $cnt = 1;
                  if ($query->rowCount() > 0) {
                    foreach ($results as $row) {
                      $assignID = $row->Assign;
                      $userID = $row->UserID;

                      $sqlUser = "SELECT * FROM tbluser WHERE ID = :userID";
                      $queryUser = $dbh->prepare($sqlUser);
                      $queryUser->bindParam(':userID', $userID, PDO::PARAM_INT);
                      $queryUser->execute();
                      $userData = $queryUser->fetch(PDO::FETCH_OBJ);
                      $sqlStaff = "SELECT * FROM tblstaff WHERE ID = :assignID";
                      $queryStaff = $dbh->prepare($sqlStaff);
                      $queryStaff->bindParam(':assignID', $assignID, PDO::PARAM_INT);
                      $queryStaff->execute();
                      $staffData = $queryStaff->fetch(PDO::FETCH_OBJ);
                  ?>
                      <table border="1" class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                        <tr>
                          <th>Booking Number</th>
                          <td><?php echo $row->BookingID; ?></td>
                          <th>Client Name</th>
                          <td><?php echo $userData->Name; ?></td>
                        </tr>


                        <tr>
                          <th>Mobile Number</th>
                          <td><?php echo $userData->MobileNumber; ?></td>
                          <th>Email</th>
                          <td><?php echo $userData->Email; ?></td>
                        </tr>
                        <tr>

                          <th>Service Date</th>
                          <td><?php echo $row->ServiceDate; ?></td>
                          <th>Service Starting Time</th>
                          <td><?php echo $row->ServiceStartingtime; ?></td>
                        </tr>
                        <tr>

                          <th>Service Ending Time</th>
                          <td><?php echo $row->ServiceEndingtime; ?></td>
                          <th>Venue Address</th>
                          <td><?php echo $row->VenueAddress; ?></td>
                        </tr>
                        <tr>

                          <th>Service Type</th>
                          <td><?php echo $row->ServiceType; ?></td>
                          <th>Additional Information</th>
                          <td><?php echo $row->AdditionalInformation; ?></td>
                        </tr>
                        <tr>

                          <th>Service Name</th>
                          <td><?php echo $row->ServiceName; ?></td>
                          <th>Service Description</th>
                          <td><?php echo $row->SerDes; ?></td>
                        </tr>
                        <tr>
                          <th>Service Price</th>
                          <td>RM<?php echo $row->ServicePrice; ?></td>
                          <th>Apply Date</th>
                          <td><?php echo $row->BookingDate; ?></td>
                        </tr>

                        <tr>

                          <th>Order Final Status</th>

                          <td> <?php
                                $assign = $row->Assign;
                                $statusID = $row->Status;
                                $sql = "SELECT * from tblstatus where ID=:id";
                                $query = $dbh->prepare($sql);
                                $query->bindParam(':id', $statusID, PDO::PARAM_INT);
                                $query->execute();
                                $status = $query->fetch(PDO::FETCH_ASSOC);

                                if ($status['Name'] == "Approved") {
                                  echo "Your Booking has been approved";
                                } else if ($status['Name'] == "Cancelled") {
                                  echo "Your Booking has been cancelled";
                                } else if ($status['Name'] == "Completed") {
                                  echo "Service has been Completed";
                                } else if ($status['Name'] == "") {
                                  echo "Not Response Yet";
                                }; ?>
                          </td>

                          <th>Admin Remark</th>

                          <?php if ($row->Remark == "") { ?>

                            <td><?php echo "Not Updated Yet"; ?></td>
                          <?php } else { ?> <td><?php echo htmlentities($row->Remark); ?>
                            </td>
                          <?php } ?>
                        </tr>
                        <tr>
                          <th>Staff Assigned</th>
                          <td><?php if ($staffData->name) echo $staffData->name;
                              else echo 'Staff Not Assigned Yet'; ?>
                          </td>
                          <th>Staff Phone</th>
                          <td><?php echo $staffData->phone_no; ?>
                          </td>
                        </tr>



                      <?php $cnt = $cnt + 1;
                    }
                      ?>
                      </table>
                      <?php

                      //if ($assign == "") {
                      ?>
                      <!-- <p align="center" style="padding-top: 20px">
                        <button class="btn btn-primary waves-effect waves-light w-lg" data-toggle="modal" data-target="#myModal">Take Action</button>
                      </p> -->

                      <?php //} 
                      ?>



                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Take Action</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <table class="table table-bordered table-hover data-tables">

                                <form method="post" name="submit">
                                  <tr>
                                    <th>Status :</th>
                                    <td>

                                      <select name="status" class="form-control wd-450" required="true">
                                        <?php
                                        $statId = $row->Status;
                                        $sql = "SELECT * from tblstatus WHERE ID=:id";
                                        $query = $dbh->prepare($sql);
                                        $query->bindParam(':id', $statId, PDO::PARAM_INT);
                                        $query->execute();
                                        $currentStatus = $query->fetch(PDO::FETCH_ASSOC);


                                        if ($row->Status) echo '<option value="' . $currentStatus['ID'] . '" selected>' . $currentStatus['Name'] . '</option>';
                                        else echo '<option value="" selected>Select Status</option>';


                                        $sql = "SELECT * from tblstatus WHERE ID != :id";
                                        $query = $dbh->prepare($sql);
                                        $query->bindParam(':id', $statId, PDO::PARAM_INT);
                                        $query->execute();
                                        $allStatus = $query->fetchAll(PDO::FETCH_ASSOC);

                                        foreach ($allStatus as $stat) {
                                          echo '<option value="' . $stat['ID'] . '">' . $stat['Name'] . '</option>';
                                        }
                                        ?>
                                      </select>
                                    </td>
                                  </tr>

                                  <tr>
                                    <th>Assign :</th>

                                    <td>
                                      <select name="assign" class="form-control wd-450">
                                        <?php
                                        $sid = $row->Assign;
                                        $role = '2';
                                        $sql = "SELECT * FROM tblstaff WHERE ID=:id ";
                                        $query = $dbh->prepare($sql);
                                        $query->bindParam(':id', $sid, PDO::PARAM_INT);
                                        $query->execute();
                                        $currentStaff = $query->fetch(PDO::FETCH_ASSOC);

                                        if ($row->Assign) echo '<option class="text-muted" value="' . $currentStaff['ID'] . '" selected>' . $currentStaff['name'] . '</option>';
                                        else echo '<option value="" selected>Select a Staff</option>';

                                        if ($row->Assign) {
                                          $sql = "SELECT * FROM tblstaff WHERE ID != :id AND role=:staffRole ";
                                          $query = $dbh->prepare($sql);
                                          $query->bindParam(':id', $sid, PDO::PARAM_INT);
                                          $query->bindParam(':staffRole', $role, PDO::PARAM_STR);
                                        } else {
                                          $sql = "SELECT * FROM tblstaff WHERE ID = :id AND role='2' ";
                                          $query = $dbh->prepare($sql);
                                          $query->bindParam(':id', $_SESSION['odmsaid'], PDO::PARAM_INT);
                                        }

                                        $query->execute();
                                        $staffMembers = $query->fetchAll(PDO::FETCH_ASSOC);

                                        foreach ($staffMembers as $staffMember) {
                                          echo '<option value="' . $staffMember['ID'] . '">' . $staffMember['name'] . '</option>';
                                        }
                                        ?>
                                      </select>
                                    </td>
                                  </tr>

                              </table>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" name="submit" class="btn btn-primary">Update</button>

                              </form>



                            </div>
                          </div>
                          <!-- END Bootstrap Register -->
                        </div>

                      </div>
                    <?php } ?>

                </div>
                <!-- END Page Content -->
              </div>
              <div class="d-flex justify-content-end my-2">
                <button class="btn btn-primary" onclick="downloadPDF()"><i class="fa fa-download fa-3" aria-hidden="true"></i></i></button>
              </div>
            </div>
          </div>
        </main>
        <!-- END Main Container -->

        <?php include_once('includes/footer.php'); ?>
      </div>
      <!-- END Page Container -->

      <!-- Codebase Core JS -->
      <script src="assets/js/core/jquery.min.js"></script>
      <script src="assets/js/core/popper.min.js"></script>
      <script src="assets/js/core/bootstrap.min.js"></script>
      <script src="assets/js/core/jquery.slimscroll.min.js"></script>
      <script src="assets/js/core/jquery.scrollLock.min.js"></script>
      <script src="assets/js/core/jquery.appear.min.js"></script>
      <script src="assets/js/core/jquery.countTo.min.js"></script>
      <script src="assets/js/core/js.cookie.min.js"></script>
      <script src="assets/js/codebase.js"></script>


      <!-- JavaScript code for PDF generation -->
      <script>
        function downloadPDF() {
          var element = document.getElementById('downloadPdf');

          // Get the BookingID from the page
          var bookingID = <?php echo json_encode($row->BookingID); ?>;

          // Create a unique filename using BookingID and current date
          var currentDate = new Date();
          var formattedDate = currentDate.toISOString().slice(0, 10);
          var filename = 'IService_' + bookingID + '_' + formattedDate + '.pdf';

          // Configure the html2pdf options
          var options = {
            margin: 10,
            filename: filename,
            image: {
              type: 'jpeg',
              quality: 0.98
            },
            html2canvas: {
              scale: 2
            },
            jsPDF: {
              unit: 'mm',
              format: 'a4',
              orientation: 'portrait'
            }
          };

          // Trigger the PDF generation with custom filename
          html2pdf(element, options);
        }
      </script>
  </body>

  </html>
<?php }  ?>