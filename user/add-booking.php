<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['odmsaid'] == 0)) {
    header('location:logout.php');
} else {
    if (isset($_POST['submit'])) {
        $bookingid = mt_rand(100000000, 999999999);
        $servicetype = $_POST['servicetype']; // Type of Service
        $eventtype = $_POST['eventtype']; // Type of Property
        $userId = $_SESSION['odmsaid'];
        $eventdesc = $_POST['eventdesc'];
        $edate = $_POST['edate'];
        $est = $_POST['est'];
        $eetime = $_POST['eetime'];
        $vaddress = $_POST['vaddress'];
        $addinfo = $_POST['addinfo'];

        $sql = "INSERT INTO tblbooking(BookingID, ServiceID, EventID, UserID, ServiceType, ServiceDate, ServiceStartingtime, ServiceEndingtime, VenueAddress, AdditionalInformation) 
                VALUES (:bookId, :servId, :eventId, :userId, :servDesc, :servDate, :servStart, :servEnd, :venAddress, :addInfo)";
        $query = $dbh->prepare($sql);
        $query->bindParam(':bookId', $bookingid, PDO::PARAM_INT);
        $query->bindParam(':servId', $servicetype, PDO::PARAM_INT);
        $query->bindParam(':eventId', $eventtype, PDO::PARAM_INT);
        $query->bindParam(':userId', $userId, PDO::PARAM_INT);
        $query->bindParam(':servDesc', $eventdesc, PDO::PARAM_STR);
        $query->bindParam(':servDate', $edate, PDO::PARAM_STR);
        $query->bindParam(':servStart', $est, PDO::PARAM_STR);
        $query->bindParam(':servEnd', $eetime, PDO::PARAM_STR);
        $query->bindParam(':venAddress', $vaddress, PDO::PARAM_STR);
        $query->bindParam(':addInfo', $addinfo, PDO::PARAM_STR);

        if ($query->execute()) {
            echo '<script>alert("Booking has been added successfully.")</script>';
            echo "<script>window.location.href = 'all-booking.php'</script>";
        } else {
            echo '<script>alert("Something Went Wrong. Please try again.")</script>';
        }
    }

?>
    <!doctype html>
    <html lang="en" class="no-focus"> <!--<![endif]-->

    <head>
        <title>I-SERVICES System - Add Property Type</title>
        <link rel="stylesheet" id="css-main" href="assets/css/codebase.min.css">
    </head>

    <body>
        <div id="page-container" class="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-narrow">


            <?php include_once('includes/sidebar.php'); ?>

            <?php include_once('includes/header.php'); ?>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">

                    <!-- Register Forms -->
                    <h2 class="content-heading">Add Booking</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Bootstrap Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-gd-emerald">
                                    <h3 class="block-title">Add Booking</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                            <i class="si si-refresh"></i>
                                        </button>
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <?php
                                    $sql = 'select * from ';
                                    ?>

                                    <form method="post">
                                        <div class="row">
                                            <div class="col-12 form-group">
                                                <label class="font-weight-normal" for="edate">Service Date:</label>
                                                <input type="date" class="form-control" name="edate" required="true">
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label class="font-weight-normal" for="servicetype">Service Type:</label>
                                                    <select type="text" class="form-control" name="servicetype" required="true">
                                                        <option value="">Choose Service Type</option>
                                                        <?php
                                                        $sql2 = "SELECT * from tblservice";
                                                        $query2 = $dbh->prepare($sql2);
                                                        $query2->execute();
                                                        $result2 = $query2->fetchAll(PDO::FETCH_OBJ);
                                                        foreach ($result2 as $row) {
                                                        ?>
                                                            <option value="<?php echo htmlentities($row->ID); ?>"><?php echo htmlentities($row->ServiceName); ?> - RM<?php echo htmlentities($row->ServicePrice); ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="font-weight-normal" for="est">Starting Time:</label>
                                                    <select type="text" class="form-control" name="est" required="true">
                                                        <option value="">Select Starting Time</option>
                                                        <option value="8 a.m">8 a.m</option>
                                                        <option value="9 a.m">9 a.m</option>
                                                        <option value="10 a.m">10 a.m</option>
                                                        <option value="11 a.m">11 a.m</option>
                                                        <option value="12 p.m">12 a.m</option>
                                                        <option value="1 p.m">1 p.m</option>
                                                        <option value="2 p.m">2 p.m</option>
                                                        <option value="3 p.m">3 p.m</option>
                                                        <option value="4 p.m">4 p.m</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="font-weight-normal" for="eetime">Finish Time:</label>
                                                    <select type="text" class="form-control" name="eetime" required="true">
                                                        <option value="">Select Finish Time</option>
                                                        <option value="10 a.m">10 a.m</option>
                                                        <option value="11 a.m">11 a.m</option>
                                                        <option value="12 p.m">12 a.m</option>
                                                        <option value="1 p.m">1 p.m</option>
                                                        <option value="2 p.m">2 p.m</option>
                                                        <option value="3 p.m">3 p.m</option>
                                                        <option value="4 p.m">4 p.m</option>
                                                        <option value="5 p.m">5 p.m</option>
                                                        <option value="6 p.m">6 p.m</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <!-- <div class="form-group">
                                                    <label class="font-weight-normal" for="eventdesc">Event Description:</label>
                                                    <input type="text" class="form-control" name="eventdesc" required="true" placeholder="Party/Gathering etc.">
                                                </div> -->
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="font-weight-normal" for="eventtype">Type of Property:</label>
                                                <select type="text" class="form-control" name="eventtype" required="true">
                                                    <option value="">Choose Property Type</option>
                                                    <?php
                                                    $sql2 = "SELECT * from tbleventtype";
                                                    $query2 = $dbh->prepare($sql2);
                                                    $query2->execute();
                                                    $result2 = $query2->fetchAll(PDO::FETCH_OBJ);
                                                    foreach ($result2 as $row) {
                                                    ?>
                                                        <option value="<?php echo htmlentities($row->ID); ?>"><?php echo htmlentities($row->EventType); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="font-weight-normal" for="vaddress">Home Address:</label>
                                                    <textarea type="text" class="form-control" name="vaddress" required="true"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="font-weight-normal" for="addinfo">Additional Information:</label>
                                                    <textarea type="text" class="form-control" name="addinfo" required="true"></textarea>
                                                </div>
                                            </div>


                                            <div class="col-12 form-group  text-right">
                                                <button type="submit" class="btn btn-alt-success" name="submit" style="width: 150px;">
                                                    <i class="fa fa-plus mr-5"></i> Add
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                            <!-- END Bootstrap Register -->
                        </div>

                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <?php include_once('includes/footer.php'); ?>
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="assets/js/core/jquery.min.js"></script>
        <script src="assets/js/core/popper.min.js"></script>
        <script src="assets/js/core/bootstrap.min.js"></script>
        <script src="assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="assets/js/core/jquery.appear.min.js"></script>
        <script src="assets/js/core/jquery.countTo.min.js"></script>
        <script src="assets/js/core/js.cookie.min.js"></script>
        <script src="assets/js/codebase.js"></script>
        <!-- Include jsPDF library -->
    </body>

    </html>
<?php }  ?>