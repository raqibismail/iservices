<?php
session_start();
error_reporting(0);
include('dbconnection.php');

if (strlen($_SESSION['odmsaid']) == 0) {
    header('location:logout.php');
} else {

    if (isset($_POST['submit'])) {
        $sql = "SELECT ServiceID from tblbooking WHERE BookingID = :bookingid";
        $query = $dbh->prepare($sql);
        $query->bindParam(':bookingid', $_GET['bookingid'], PDO::PARAM_INT);
        $query->execute();
        $results = $query->fetch(PDO::FETCH_ASSOC);
        $sql = "SELECT ServicePrice from tblservice WHERE ID = :serviceID";
        $query = $dbh->prepare($sql);
        $query->bindParam(':serviceID', $results['ServiceID'], PDO::PARAM_INT);
        $query->execute();
        $results = $query->fetch(PDO::FETCH_ASSOC);

        $bookingID = $_GET['bookingid'];
        $cardNumber = $_POST['card_number'];
        $cardName = $_POST['card_name'];
        $cardExpDate = $_POST['card_exp_date'];
        $cardCvc = $_POST['card_cvc'];
        $priceTotal = $results['ServicePrice'];
        $userID = $_SESSION['odmsaid'];
        $paymentDate = date("Y-m-d H:i:s"); // This format is "YYYY-MM-DD HH:MM:SS"

        echo "Card Number: $cardNumber<br>";
        echo "Card Name: $cardName<br>";
        echo "Card Expiry Date: $cardExpDate<br>";
        echo "Card CVC: $cardCvc<br>";
        echo "Price Total: $priceTotal<br>";
        echo "User ID: $userID<br>";

        // Generate paymentID
        $currentDate = date("dmY");
        $randomNumbers = mt_rand(1000, 9999);
        $generatedID = $currentDate . $userID . $randomNumbers;

        // Insert data into the 'payment' table
        $sql = "INSERT INTO tblpayment (paymentID, cardNumber, cardName, cardExpDate, cardCVC, priceTotal, paymentDate, bookingID, userID) 
        VALUES (:paymentID, :cardNumber, :cardName, :cardExpDate, :cardCvc, :priceTotal, :paymentDate, :bookingID, :userID)";
        $query = $dbh->prepare($sql);
        $query->bindParam(':paymentID', $generatedID, PDO::PARAM_STR);
        $query->bindParam(':cardNumber', $cardNumber, PDO::PARAM_STR);
        $query->bindParam(':cardName', $cardName, PDO::PARAM_STR);
        $query->bindParam(':cardExpDate', $cardExpDate, PDO::PARAM_STR);
        $query->bindParam(':cardCvc', $cardCvc, PDO::PARAM_STR);
        $query->bindParam(':priceTotal', $priceTotal, PDO::PARAM_STR);
        $query->bindParam(':paymentDate', $paymentDate, PDO::PARAM_STR);
        $query->bindParam(':bookingID', $bookingID, PDO::PARAM_STR);
        $query->bindParam(':userID', $userID, PDO::PARAM_INT);


        if ($query->execute()) {
            $lastInsertedId = $generatedID;
            echo '<script type="text/javascript">';
            echo 'alert("Payment successful");';
            echo 'window.location.href = "../payment-view.php?id=' . $lastInsertedId . '";';  // Redirect to another page with the ID parameter
            echo '</script>';
        } else {
            // Error storing payment information
            $errorInfo = $query->errorInfo();
            echo '<script type="text/javascript">';
            echo 'alert("Error: ' . $errorInfo[2] . '");';
            echo 'window.location.href = "../payment.php";';  // Redirect to another page if needed
            echo '</script>';
        }
    } else {
        echo '<script type="text/javascript">';
        echo 'alert("Error: Form not submitted.");';
        echo 'window.location.href = "../payment.php";';  // Redirect to another page if needed
        echo '</script>';
    }
}
