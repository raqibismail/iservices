<?php
session_start();
error_reporting(0);
include('dbconnection.php');

if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    // SQL query to check if the username and password match a record in the database
    $sql = "SELECT ID FROM tblstaff WHERE username = :username AND password = :password";
    $query = $dbh->prepare($sql);
    $query->bindParam(':username', $username, PDO::PARAM_STR);
    $query->bindParam(':password', $password, PDO::PARAM_STR);
    $query->execute();
    
    // Fetch the results from the query
    $results = $query->fetchAll(PDO::FETCH_OBJ);
    
    if ($query->rowCount() > 0) {
        foreach ($results as $result) {
            $_SESSION['odmsaid'] = $result->ID;
    
            // Retrieve the role from the database for the logged-in user
            $sqlRole = "SELECT role FROM tblstaff WHERE ID = :userID";
            $queryRole = $dbh->prepare($sqlRole);
            $queryRole->bindParam(':userID', $result->ID, PDO::PARAM_INT);
            $queryRole->execute();
            $userRole = $queryRole->fetchColumn(); // Assuming 'Role' is a column in the tblstaff table
    
            if (!empty($_POST["remember"])) {
                // Set cookies for username and password if the "Remember Me" checkbox is checked
                setcookie("user_login", $_POST["username"], time() + (10 * 365 * 24 * 60 * 60));
                setcookie("userpassword", $_POST["password"], time() + (10 * 365 * 24 * 60 * 60));
            } else {
                // Clear cookies if "Remember Me" is unchecked
                if (isset($_COOKIE["user_login"])) {
                    setcookie("user_login", "");
                }
                if (isset($_COOKIE["userpassword"])) {
                    setcookie("userpassword", "");
                }
            }
    
            $_SESSION['login'] = $_POST['username'];
    
            // Redirect to different pages based on the user's role
            if ($userRole === 'admin') {
                echo "<script type='text/javascript'> document.location = '../dashboard.php'; </script>";
            } elseif ($userRole === 'staff') {
                echo "<script type='text/javascript'> document.location = '../../staff/dashboard.php'; </script>";
            }
        }
    } else {
        // Display an error message for invalid credentials
        echo "<script>alert('Invalid Details');</script>";
    }    
}
?>
