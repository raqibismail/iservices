<?php
session_start();
error_reporting(0);

include('includes/dbconnection.php');
?>
<!DOCTYPE html>
<html>

<head>
	<title>I-SERVICES System || About Us</title>
	<link href='http://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<!-- Your custom CSS -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all">

</head>

<body>
	<!---->
	<?php include_once('includes/header.php'); ?>
	<div class="about content">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li class="active">About</li>
			</ol>
			<?php
			$sql = "SELECT * from tblpage where PageType='aboutus'";
			$query = $dbh->prepare($sql);
			$query->execute();
			$results = $query->fetchAll(PDO::FETCH_OBJ);
			$aboutUs = $query->fetch(PDO::FETCH_OBJ);
			$cnt = 1;
			if ($query->rowCount() > 0) {
				foreach ($results as $row) {               ?>
					<h2><?php echo htmlentities($row->PageTitle); ?></h2>
					<div class="about-main">
						<div class="col-md-6">
							<img src="images/cleaning.jpg" class="img-responsive" alt="" />
						</div>
						<div class="col-md-6 abt-pic-info">

							<p style="color:#ffffff"><?php echo $row->PageDescription; ?>.</p>

						</div>
				<?php $cnt = $cnt + 1;
				}
			} ?>
				<div class="clearfix"></div>
					</div>

					<div class="latest">
						<h3>SERVICES</h3>
						<div class="pic start">
							<img src="images/cleaning5.jpg" class="img-style row6" alt="">
							<h4><a class="text-white" href="services.php">Cleaning Yards</a></h4>
							<p></p>
						</div>
						<div class="pic">
							<img src="images/cleaningmop.jpg" class="img-style row6" alt="">
							<h4><a class="text-white" href="services.php">Cleaning</a></h4>
							<p></p>
						</div>
						<div class="pic">
							<img src="images/cleaningfolding.jpg" class="img-style row6" alt="">
							<h4><a class="text-white" href="services.php">Folding Clothes</a></h4>
							<p></p>
						</div>
						<div class="pic">
							<img src="images/cleaning3.jpg" class="img-style row6" alt="">
							<h4><a class="text-white" href="services.php">Laundry</a></h4>
							<p></p>
						</div>
						<div class="pic">
							<img src="images/cleaning6.jpg" class="img-style row6" alt="">
							<h4><a class="text-white" href="services.php">Babysitting</a></h4>
							<p></p>
						</div>
						<div class="clearfix"></div>
					</div>
		</div>
	</div>
	<!---->

	<!---->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
	<script src="js/jquery.min.js"></script>
	<script type="application/x-javascript">
		addEventListener("load", function() {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<script type="text/javascript" src="js/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

		});
	</script>
</body>

<footer>
	<div class="container-flex">
		<?php include_once('includes/footer.php'); ?>
	</div>
</footer>

</html>