<!doctype html>
<html lang="en" class="no-focus"> <!--<![endif]-->

<head>
    <title>I-SERVICES</title>
    <link rel="stylesheet" id="css-main" href="assets/css/codebase.min.css">
</head>

<body>
    <div id="page-container" class="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-narrow">

        <?php include_once('includes/sidebar.php'); ?>

        <?php include_once('includes/header.php'); ?>


        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Row #1 -->
                <div class="row gutters-tiny invisible" data-toggle="appear">
                    <div class="col-6 col-md-4 col-xl-4">
                        <a class="block text-center" href="all-booking.php">
                            <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-dusk">
                                <?php
                                $sql = "SELECT * from tblbooking";
                                $query = $dbh->prepare($sql);
                                $query->execute();
                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                $totalnewbooking = $query->rowCount();
                                ?>
                                <div class="ribbon-box"><?php echo htmlentities($totalnewbooking); ?></div>
                                <p class="mt-5">
                                    <i class="si si-pencil fa-3x text-white-op"></i>
                                </p>
                                <p class="font-w600 text-white">Total All Booking</p>
                            </div>
                        </a>
                    </div>
                    
                    <div class="col-6 col-md-4 col-xl-4">
                        <a class="block text-center" href="assigned-task.php">
                            <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-dusk">
                                <?php
                                $assignId = $_SESSION['odmsaid'];
                                $sql = "SELECT * from tblbooking WHERE Assign = :assignId";
                                $query = $dbh->prepare($sql);
                                $query->bindParam(':assignId', $assignId, PDO::PARAM_INT);
                                $query->execute();
                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                $totalassignedtask = $query->rowCount();
                                ?>
                                <div class="ribbon-box"><?php echo htmlentities($totalassignedtask); ?></div>
                                <p class="mt-5">
                                    <i class="si si-target fa-3x text-white-op"></i>
                                </p>
                                <p class="font-w600 text-white">Total Assigned Tasks</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-6 col-md-4 col-xl-4">
                        <a class="block text-center" href="available-task.php">
                            <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-sea">
                                <?php
                                $assignId = $_SESSION['odmsaid'];
                                $sql = "SELECT * from tblbooking WHERE (Assign = '' OR Assign IS NULL) AND (Status = 1 OR (Status = '' OR Status IS NULL))";
                                $query = $dbh->prepare($sql);
                                $query->execute();
                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                $totalavailabletask = $query->rowCount();
                                ?>
                                <div class="ribbon-box"><?php echo htmlentities($totalavailabletask); ?></div>
                                <p class="mt-5">
                                    <i class="si si-support fa-3x text-white-op"></i>
                                </p>
                                <p class="font-w600 text-white">Total Available Tasks</p>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-6 col-md-4 col-xl-4">
                        <a class="block text-center" href="manage-services.php">
                            <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-dusk">
                                <?php
                                $sql = "SELECT ID from tblservice";
                                $query = $dbh->prepare($sql);
                                $query->execute();
                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                $totalserv = $query->rowCount();
                                ?>
                                <div class="ribbon-box"><?php echo htmlentities($totalserv); ?></div>
                                <p class="mt-5">
                                    <i class="si si-wallet fa-3x text-white-op"></i>
                                </p>
                                <p class="font-w600 text-white">Total Services</p>
                            </div>
                        </a>
                    </div> -->
                    <!-- <div class="col-6 col-md-4 col-xl-4">
                        <a class="block text-center" href="manage-event-type.php">
                            <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-dusk">
                                <?php
                                $sql = "SELECT ID from tbleventtype";
                                $query = $dbh->prepare($sql);
                                $query->execute();
                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                $totaleventtype = $query->rowCount();
                                ?>
                                <div class="ribbon-box"><?php echo htmlentities($totaleventtype); ?></div>
                                <p class="mt-5">
                                    <i class="si si-bubbles fa-3x text-white-op"></i>
                                </p>
                                <p class="font-w600 text-white">Total Property Type</p>
                            </div>
                        </a>
                    </div> -->


                    <!-- <div class="col-6 col-md-4 col-xl-4">
                        <a class="block text-center" href="manage-staff.php">
                            <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-sea">
                                <?php
                                $sql = "SELECT ID from tblstaff WHERE role = '2'";
                                $query = $dbh->prepare($sql);
                                $query->execute();
                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                foreach ($results as $row) {
                                    $countStaff++;
                                }
                                ?>
                                <div class="ribbon-box"><?php echo $countStaff ?></div>
                                <p class="mt-5">
                                    <i class="si si-people fa-3x text-white-op"></i>
                                </p>
                                <p class="font-w600 text-white">Total Staff</p>
                            </div>
                        </a>
                    </div> -->
                    <div class="col-6 col-md-4 col-xl-4">
                        <a class="block text-center" href="manage-user.php">
                            <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-dusk">
                                <?php
                                $sql = "SELECT ID from tbluser";
                                $query = $dbh->prepare($sql);
                                $query->execute();
                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                $countUser = $query->rowCount();
                                ?>
                                <div class="ribbon-box"><?php echo htmlentities($countUser); ?></div>
                                <p class="mt-5">
                                    <i class="si si-bubbles fa-3x text-white-op"></i>
                                </p>
                                <p class="font-w600 text-white">Total User</p>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- END Row #2 -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <?php include_once('includes/footer.php'); ?>
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="assets/js/core/jquery.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <script src="assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="assets/js/core/jquery.appear.min.js"></script>
    <script src="assets/js/core/jquery.countTo.min.js"></script>
    <script src="assets/js/core/js.cookie.min.js"></script>
    <script src="assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="assets/js/plugins/chartjs/Chart.bundle.min.js"></script>

    <!-- Page JS Code -->
    <script src="assets/js/pages/be_pages_dashboard.js"></script>
</body>

</html>