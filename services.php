<?php
session_start();
error_reporting(0);

include('includes/dbconnection.php');
?>

<!DOCTYPE html>
<html>

<head>
	<title>I-SERVICES System || Housekeeping Services</title>
	<link href='http://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<!-- Your custom CSS -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
</head>

<body>
	<?php include_once('includes/header.php'); ?>
	<div class="event content">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li class="active">Services</li>
			</ol>
			<h2>Services</h2>
			<div class="event-main">
				<?php
				$sql = "SELECT * from tblservice";
				$query = $dbh->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(PDO::FETCH_OBJ);

				$cnt = 1;
				if ($query->rowCount() > 0) {
				?>
					<table class="table table-bordered text-white  align-middle">
						<thead>
							<tr class="text-center">
								<th class="align-middle">Service Name</th>
								<th class="align-middle">Description</th>
								<th class="align-middle">Price (RM)</th>
								<!-- <th class="align-middle">Action</th> -->
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($results as $row) {
							?>
								<tr>
									<td class="text-center"><?php echo htmlentities($row->ServiceName); ?></td>
									<td><?php echo htmlentities($row->SerDes); ?></td>
									<td>RM<?php echo htmlentities($row->ServicePrice); ?></td>
								</tr>
							<?php
								$cnt = $cnt + 1;
							}
							?>
						</tbody>
					</table>
				<?php } ?>
			</div>
			<div class="text-center pt-3">
				<a href="book-services.php" class="btn btn-primary">Register Here</a>
			</div>
		</div>
	</div>
	<footer>
		<div class="container-flex">
			<?php include_once('includes/footer.php'); ?>
		</div>
	</footer>
	<script src="js/jquery.min.js"></script>
	<script type="application/x-javascript">
		addEventListener("load", function() {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
</body>

</html>